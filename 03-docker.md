Dockerの導入
===


```sh
sudo apt-get update

# 事前必要なソフト諸々インストール
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

# docker公式のGPG公開鍵をインストール
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# dockerのインストール
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt-get update
sudo apt-get install -y docker-ce

# 一般ユーザでも使えるように設定
sudo usermod -aG docker vagrant

# 確認
docker -v
  => Docker version 19.03.12, build 48a66213fe # こんな感じで出ればOK
```


## docker-composeの導入

```sh
# インストール
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# 権限の変更
sudo chmod +x /usr/local/bin/docker-compose

# 確認
docker-compose --version
  => docker-compose version 1.26.2, build eefe0d31 # こんな感じで出ればOK
```
