### コンテナ起動
```sh
docker-compose up -d

# 起動しているかどうか確認
docker-compose ps
  Name     Command   State   Ports
  ---------------------------------
  python3   python3   Up
```

### コンテナにログイン
```
docker-compose exec python3 bash
```

### Pythonスクリプト実行
```sh
# コンテナにログイン後
python main.rb
```

### pipでライブラリを追加するとき
```sh
# コンテナを終了する
docker-compose down

# Dockerfileの最終行にコマンドを追加する
# ex) RUN pip install <library>

# コンテナの再起動
docker-compose up -d --build

# インストールされているか確認するにはコンテナにログインして以下のコマンドで確認できる
pip list
```
